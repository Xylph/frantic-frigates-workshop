﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance = null;

    public int Score;
    public int RequiredScore;
    public GameObject Player;

    private void Awake()
    {
        Instance = this;
    }

    private void OnDestroy()
    {
        Instance = null;
    }

    private void Update()
    {
        // Check for win condition
        if (Score >= RequiredScore)
        {
            Score = RequiredScore;
            SceneManager.LoadSceneAsync("Victory");
        }

        // Check for lose condition
        if (!Player)
        {
            SceneManager.LoadSceneAsync("GameOver");
        }
    }
}

