﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform Target;
    public float Speed = 1.0f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Target)
        {
            // Slowly follow target
            Vector3 newPos = Vector3.Lerp(transform.position, Target.position, Speed * Time.deltaTime);
            newPos.z = transform.position.z; // Preserve z since we're in 2D
            transform.position = newPos;
        }
    }
}
