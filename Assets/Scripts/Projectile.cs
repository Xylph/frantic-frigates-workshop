﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public int Damage = 1;
    public float Speed = 10.0f;
    public Vector3 Direction;
    public float DestroyAfterDelay;

    private void Start()
    {
        Destroy(gameObject, DestroyAfterDelay);
    }

    void Update()
    {
        // Move towards specified direction
        transform.position += Direction * Speed * Time.deltaTime;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Check for opposing team
        if (tag != collision.tag)
        {
            Health health = collision.GetComponent<Health>();
            // Check if what we're hitting has health
            if (health)
            {
                // Apply damage
                health.TakeDamage(Damage);

                // Destroy the projectile here
                Destroy(gameObject);
            }
        }
    }
}
